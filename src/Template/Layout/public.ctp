<!doctype html>
<html lang="en">
    <head>
        <title>Typing knowledge desk</title>
        <!-- Required meta tags -->
        <?= $this->Html->charset() ?>
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <!-- Favicon -->
        <link rel="shortcut icon" href="/img/favicon.png" />
        <!-- bootstrap -->
        <?= $this->Html->css('https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css') ?>
        <?= $this->Html->css('https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css') ?>
        <!-- REVOLUTION STYLE SHEETS -->
        <?= $this->Html->css('/revolution/css/settings') ?>
        <!-- megamenu -->
        <?= $this->Html->css('mega-menu/mega_menu') ?>
        <!-- owl carousel-->
        <?= $this->Html->css('owl-carousel/owl.carousel') ?>
        <!-- shortcodes -->
        <?= $this->Html->css('shortcodes') ?>
        <!-- main style -->
        <?= $this->Html->css('style') ?>
        <!-- responsive -->
        <?= $this->Html->css('responsive') ?>

    </head>

    <body>
        <!-- loading -->
        <div id="loading">
            <div id="loading-center">
                <?= $this->Html->image('loader.png', ['alt' => 'loder']) ?>
            </div>
        </div>
        <!-- loading End -->
        <!-- HEADER  -->
        <header class="header-01 re-none">
            <div class="topbar">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-6">
                            <div class="topbar-left">
                                <ul class="list-inline">
                                    <li class="list-inline-item"><i class="fa fa-phone text-blue"></i> +91 99 02 456 789</li>
                                    <li class="list-inline-item"><i class="fa fa-envelope-o"> </i> <a href="#">info@typingknowlededesk.com</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6">
                            <div class="topbar-right text-right">
                                <ul class="list-inline">
                                    <li class="list-inline-item">
                                        <ul class="list-inline iq-left">
                                            <li class="list-inline-item"><a href="#" data-toggle="modal" data-target=".iq-login-from" data-whatever="@mdo"><i class="fa fa-lock"></i>Login</a></li>
                                            <li class="list-inline-item"><a href="#" data-toggle="modal" data-target=".iq-register-from" data-whatever="@fat"><i class="fa fa-user"></i>Sign Up</a></li>
                                        </ul>
                                    <li class="list-inline-item"><a href="#"><i class="fa fa-users"></i>Number of Users </a></li>
                                </ul>
                                </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="topbar-news">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <div class="owl-carousel re-inherit" data-autoplay="true" data-loop="true" data-nav="false" data-dots="false" data-items="1" data-items-laptop="1" data-items-tab="1" data-items-mobile="1" data-items-mobile-sm="1">
                                <div class="item">
                                    <p class="breaking-news"><span class="breaking-news-left">Breaking News</span><span class="breaking-news-right">गुरुग्राम में कोरोना के 85 नए मामले, अब तक 4512 लोग संक्रमित</span></p>
                                </div>
                                <div class="item">
                                    <p class="breaking-news"><span class="breaking-news-left">Breaking News</span><span class="breaking-news-right"> India-China military commanders’ meeting at LAC ends, may resume on Tuesday</span></p>
                                </div>
                                <div class="item">
                                    <p class="breaking-news"><span class="breaking-news-left">Breaking News</span><span class="breaking-news-right">चीन से LAC पर तनाव दूर करने को लेकर 11 घंटे तक हुई कमांडर स्तर की वार्ता</span></p>
                                </div>
                                <div class="item">
                                    <p class="breaking-news"><span class="breaking-news-left">Breaking News</span><span class="breaking-news-right"> Over 2,900 new cases take Delhi's Covid tally to 62,655</span></p>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- menu start -->
            <nav id="menu-1" class="mega-menu">
                <!-- menu list items container -->
                <section class="menu-list-items">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-sm-12">
                                <!-- menu logo -->
                                <ul class="menu-logo">
                                    <li>
                                        <a href="index.html">
                                            <?= $this->Html->image('logo.png', ['alt' => 'logo']) ?>
                                        </a>
                                    </li>
                                </ul>
                                <ul class="menu-sidebar pull-right">
                                    <li class="iq-share">
                                        <div class="slideouticons">
                                            <input type="checkbox" id="togglebox" />
                                            <label for="togglebox" class="mainlabel"><i class="fa fa-share-alt"></i></label>
                                            <div class="iconswrapper">
                                                <ul>
                                                    <li><a href="#"><i class="fa fa-facebook" title="Facebook"></i></a></li>
                                                    <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                                                    <li><a href="#"><i class="fa fa-google-plus" title="Google Plus"></i></a></li>
                                                    <li><a href="#"><i class="fa fa-instagram" title="Instagram"></i></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                                <!-- menu links -->
                                <ul class="menu-links">
                                    <!-- active class -->
                                    <li class="active"><a href="#">Home</a></li>   
                                    <li class=""><a href="#">Keyboard</a>
                                        <!-- drop down multilevel  -->
                                        <ul class="drop-down-multilevel">

                                            <li><a href="#" class="">Hindi Keyboard</a></li>
                                            <li><a href="#" class="">Hindi Remington</a></li>
                                            <li><a href="#" class="">Punjabi Keyboard</a></li>
                                            <li><a href="#" class="">Bengali Keyboard</a></li>
                                            <li><a href="#" class="">Kannada Keyboard</a></li>
                                            <li><a href="#" class="">Gujarati Keyboard</a></li>
                                            <li><a href="#" class="">Marathi Keyboard</a></li>
                                            <li><a href="#" class="">Malayalam Keyboard</a></li>
                                            <li><a href="#" class="">Telugu Keyboard</a></li>
                                            <li><a href="#" class="">Tamil Keyboard</a></li>
                                            <li><a href="#" class="">Oriya Keyboard</a></li>
                                            <li><a href="#">Arabic Keyboard</a></li>

                                        </ul>
                                    <li><a href="#">Typing </a>
                                        <!-- drop down multilevel  -->
                                        <ul class="drop-down-multilevel">
                                            <li><a href="#" class="">Hindi Typing</a></li>
                                            <li><a href="#" class="">Punjabi Typing</a></li>
                                            <li><a href="#" class="">Bengali Typing</a></li>
                                            <li><a href="#" class="">Kannada Typing</a></li>
                                            <li><a href="#" class="">Telugu Typing</a></li>
                                            <li><a href="#" class="">Gujarati Typing</a></li>	
                                            <li><a href="#" class="">Tamil Typing</a></li>
                                            <li><a href="#" class="">Marathi Typing</a></li>
                                            <li><a href="#" class="">Malayalam Typing</a></li>
                                            <li><a href="#" class="">Oriya Typing</a></li>
                                            <li><a href="#">Arabic Typing</a></li>
                                        </ul>
                                    </li>
                                    <li><a href="#">Typing Tutor</a>
                                        <!-- drop down multilevel  -->
                                        <ul class="drop-down-multilevel">
                                            <li><a href="#" class="">English Typing Tutor</a></li>
                                            <li><a href="#" class="">Hindi Typing Tutor KrutiDev</a></li>
                                            <li><a href="#" class="">Hindi Typing Tutor Mangal Inscript</a></li>
                                            <li><a href="#">Hindi Typing Tutor Old</a></li>

                                        </ul>
                                    </li>
                                    <li><a href="#">Typing Test</a>
                                        <!-- drop down multilevel  -->
                                        <ul class="drop-down-multilevel">
                                            <li><a href="#" class="">Hindi Typing Test</a></li>
                                            <li><a href="#" class="">Hindi Typing Test [Char]</a></li>
                                            <li><a href="#" class="">Hindi Typing Test (Mangal)</a></li>
                                            <li><a href="#" class="">English Typing Test</a></li>
                                        </ul>

                                    </li>
                                    <li><a href="#">Translator</a>
                                        <!-- drop down multilevel  -->
                                        <ul class="drop-down-multilevel">
                                            <li><a href="#" class="">English to Hindi</a></li>
                                            <li><a href="#" class="">English to Punjabi</a></li>
                                            <li><a href="#" class="">English to Bengali</a></li>
                                            <li><a href="#" class="">English to Kannada</a></li>
                                            <li><a href="#" class="">English to Telugu</a></li>
                                            <li><a href="#" class="">English to Gujarati</a></li>
                                            <li><a href="#" class="">English to Tamil</a></li>
                                            <li><a href="#" class="">English to Marathi</a></li>
                                            <li><a href="#" class="">English to Malayalam</a></li>
                                        </ul>
                                    </li>
                                    <li><a href="#">Converter</a>
                                        <!-- drop down multilevel  -->
                                        <ul class="drop-down-multilevel">
                                            <li><a href="#" class="">Krutidev to Unicode</a></li>
                                            <li><a href="#" class="">Unicode to Krutidev</a></li>
                                            <li><a href="#" class="">Chanakya to Unicode</a></li>
                                            <li><a href="#" class="">Unicode to Chanakya</a></li>
                                            <li><a href="#" class="">4cGandhi to Unicode</a></li>
                                            <li><a href="#" class="">Unicode to 4cGandhi</a></li>

                                        </ul>
                                    </li>
                                    <li><a href="#">Downloads</a>
                                        <!-- drop down multilevel  -->
                                        <ul class="drop-down-multilevel">
                                            <li><a href="#" class="">Hindi Fonts</a></li>
                                            <li><a href="#">Android App <i class="fa fa-android" aria-hidden="true"></i></a></li>

                                        </ul>
                                    </li>
                                    <li><a href="#">Blog</a></li>
                                    <li><a href="#">Game</a>
                                        <!-- drop down multilevel  -->
                                        <ul class="drop-down-multilevel">
                                            <li><a href="#" class="">Tank War ( English ) </a></li>
                                            <li><a href="#" class="">Tank War ( Hindi )</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </section>
            </nav>
        </header>
        <!-- /HEADER END -->


        <?= $this->Flash->render() ?>
        <?= $this->fetch('content') ?>
        <!--=================================
            Footer-->
        <footer class="iq-footer4">
            <div class="container">
                <div class="row overview-block-ptb2">
                    <div class="col-lg-4 col-md-6 col-sm-6 iq-mtb-20">
                        <div class="logo">
                            <?= $this->Html->image('logo.png', ['class' => 'img-fluid', 'id' => 'footer_logo_img', 'alt' => '#']) ?>
                            <div class="iq-font-white iq-mt-15">
                                <div class="iq-media-blog">
                                    <ul class="list-inline">
                                        <li class="d-inline-block"><a href="#"><i class="fa fa-facebook"></i></a></li>
                                        <li class="d-inline-block"><a href="#"><i class="fa fa-twitter"></i></a></li>
                                        <li class="d-inline-block"><a href="#"><i class="fa fa-google-plus" title="Google Plus"></i></a></li>
                                        <li class="d-inline-block"><a href="#"><i class="fa fa-instagram" title="Instagram"></i></a></li>
                                        <li class="d-inline-block"><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-6 iq-mtb-20">
                        <h5 class="small-title iq-tw-6 iq-font-white iq-mb-20">Quick Links</h5>
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-6">
                                <ul class="menu">
                                    <li><a href="#">Home</a></li>
                                    <li><a href="#">Start Typing Test</a></li>
                                    <li><a href="#">Forum</a></li>
                                    <li><a href="#">Feedback</a></li>
                                </ul>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6">
                                <ul class="menu">
                                    <li><a href="#">FAQ</a></li>
                                    <li><a href="#">Support</a></li>
                                    <li><a href="#">Contact</a></li>
                                    <li><a href="#">Blog</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-6 iq-mtb-20">
                        <div class="contact-bg">
                            <h5 class="small-title iq-tw-6 iq-font-white iq-mb-30">Address</h5>
                            <ul class="iq-contact">
                                <li>
                                    <i class="ion-ios-location-outline"></i>
                                    <p>Delhi, India</p>
                                </li>
                                <li>
                                    <i class="ion-ios-telephone-outline"></i>
                                    <p>+91 88 02 03 546 456</p>
                                </li>
                                <li>
                                    <i class="ion-ios-email-outline"></i>
                                    <p>info@typingknowledgedesk.com</p>
                                </li>
                            </ul>
                        </div>
                    </div>

                </div>
                <hr>
                <div class="row overview-block-ptb2">
                    <div class="col-lg-6 col-md-6 col-sm-12">
                        <ul class="link">
                            <li class="d-inline-block iq-mr-20"><a href="#">Term and Condition</a></li>
                            <li class="d-inline-block"><a href="#"> Privacy Policy</a></li>
                        </ul>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12">
                        <div class="iq-copyright">
                            Copyright 
                            <span id="copyright">
                                <script>document.getElementById('copyright').appendChild(document.createTextNode(new Date().getFullYear()))</script>
                            </span>
                            <a href="#">Typingknowledgedesk.com</a> All Rights Reserved 
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        <!--=================================
           Footer -->



        <!--=================================
             Login -->
        <div class="modal fade iq-login-from" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header text-center">
                        <h4 class="modal-title iq-tw-5">Login</h4>
                        <a class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times; </span>
                        </a>
                    </div>
                    <div class="modal-body">
                        <form>
                            <div class="form-group">
                                <input type="text" class="form-control" id="recipient-name" placeholder="Email">
                            </div>
                            <div class="form-group">
                                <input type="password" class="form-control" id="recipient-password" placeholder="Password">
                            </div>
                            <a class="button iq-mtb-10" href="#">Login</a>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-check">
                                        <label class="form-check-label">
                                            <input type="checkbox" class="form-check-input">Remember Me</label>
                                    </div>
                                </div>
                                <div class="col-sm-6 text-right">
                                    <a href="#">Forgot Password</a>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer text-center">
                        <div> Don't Have an Account? <a href="#" class="iq-font-yellow">Register Now</a></div>
                        <ul class="iq-media-blog iq-mt-20">
                            <li><a href="# "><i class="fa fa-twitter "></i></a></li>
                            <li><a href="# "><i class="fa fa-facebook "></i></a></li>
                            <li><a href="# "><i class="fa fa-google "></i></a></li>
                            <li><a href="# "><i class="fa fa fa-linkedin "></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!--=================================
             Login -->
        <!--=================================
             Register -->
        <div class="modal fade iq-register-from" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header text-center">
                        <h4 class="modal-title iq-tw-5">Create Account</h4>
                        <a class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </a>
                    </div>
                    <div class="modal-body">
                        <form>
                            <div class="form-group">
                                <input type="text" class="form-control" id="recipient-username" placeholder="User Name">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" id="recipient-email" placeholder="Email">
                            </div>
                            <div class="form-group">
                                <input type="password" class="form-control" id="password" placeholder="Password">
                            </div>
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input type="checkbox" class="form-check-input">I Agree to the Terms and Conditions</label>
                            </div>
                            <p> Sign Up, you agree to our Terms, Data Policy and Cookie Policy. You may receive Email notifications from us</p>
                            <a class="button iq-mtb-10" href="#">Sign Up</a>
                        </form>
                    </div>
                    <div class="modal-footer text-center">
                        <div> Already Have an Account? <a href="#" class="iq-font-yellow">Login</a></div>
                        <ul class="iq-media-blog iq-mt-20">
                            <li><a href="# "><i class="fa fa-twitter "></i></a></li>
                            <li><a href="# "><i class="fa fa-facebook "></i></a></li>
                            <li><a href="# "><i class="fa fa-google "></i></a></li>
                            <li><a href="# "><i class="fa fa-linkedin"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!--=================================
             Register -->
        <!-- back-to-top -->
        <div id="back-to-top">
            <a class="top" id="scrollUp" href="#top"> <i class="fa fa-angle-up"></i> </a>
        </div>
        <!-- back-to-top End -->

        <!-- Jquery  -->
        <?= $this->Html->script('jquery.min') ?>
        <!--================ Bootstrap  =================-->
        <?= $this->Html->script('https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js') ?>
        <?= $this->Html->script('https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js') ?>
        <!-- Mega Menu -->
        <?= $this->Html->script('mega-menu/mega_menu') ?>
        <!-- Main -->
        <?= $this->Html->script('main') ?>
        <!-- REVOLUTION JS FILES -->
        <?= $this->Html->script('/revolution/js/jquery.themepunch.tools.min') ?>
        <?= $this->Html->script('/revolution/js/jquery.themepunch.revolution.min') ?>
        <!-- SLIDER REVOLUTION 5.0 EXTENSIONS  (Load Extensions only on Local File Systems !  The following part can be removed on Server for On Demand Loading) -->
        <?= $this->Html->script('/revolution/js/extensions/revolution.extension.layeranimation.min') ?>
        <?= $this->Html->script('/revolution/js/extensions/revolution.extension.navigation.min') ?>
        <?= $this->Html->script('/revolution/js/extensions/revolution.extension.slideanims.min') ?>
        <?= $this->Html->script('custom') ?>
        <!-- END REVOLUTION SLIDER -->
        <script>
            var revapi126, tpj;
            (function () {
                if (!/loaded|interactive|complete/.test(document.readyState))
                    document.addEventListener("DOMContentLoaded", onLoad);
                else
                    onLoad();

                function onLoad() {
                    if (tpj === undefined) {
                        tpj = jQuery;
                        if ("off" == "on")
                            tpj.noConflict();
                    }
                    if (tpj("#rev_slider_126_1").revolution == undefined) {
                        revslider_showDoubleJqueryError("#rev_slider_126_1");
                    } else {
                        revapi126 = tpj("#rev_slider_126_1").show().revolution({
                            sliderType: "standard",
                            jsFileLocation: "",
                            sliderLayout: "fullwidth",
                            dottedOverlay: "none",
                            delay: 9000,
                            navigation: {
                                keyboardNavigation: "off",
                                keyboard_direction: "horizontal",
                                mouseScrollNavigation: "off",
                                mouseScrollReverse: "default",
                                onHoverStop: "off",
                                arrows: {
                                    style: "zeus",
                                    enable: true,
                                    hide_onmobile: false,
                                    hide_onleave: false,
                                    tmp: '<div class="tp-title-wrap">    <div class="tp-arr-imgholder"></div> </div>',
                                    left: {
                                        h_align: "left",
                                        v_align: "center",
                                        h_offset: 20,
                                        v_offset: 0
                                    },
                                    right: {
                                        h_align: "right",
                                        v_align: "center",
                                        h_offset: 20,
                                        v_offset: 0
                                    }
                                }
                            },
                            visibilityLevels: [1240, 1024, 778, 480],
                            gridwidth: 1170,
                            gridheight: 790,
                            lazyType: "none",
                            shadow: 0,
                            spinner: "spinner0",
                            stopLoop: "off",
                            stopAfterLoops: -1,
                            stopAtSlide: -1,
                            shuffle: "off",
                            autoHeight: "off",
                            disableProgressBar: "on",
                            hideThumbsOnMobile: "off",
                            hideSliderAtLimit: 0,
                            hideCaptionAtLimit: 0,
                            hideAllCaptionAtLilmit: 0,
                            debugMode: false,
                            fallbacks: {
                                simplifyAll: "off",
                                nextSlideOnWindowFocus: "off",
                                disableFocusListener: false,
                            }
                        });
                    }
                    ; /* END OF revapi call */
                }
                ; /* END OF ON LOAD FUNCTION */
            }()); /* END OF WRAPPING FUNCTION */
        </script>
    </body>
</html>
