<!--=================================
Banner -->
<div id="rev_slider_126_1_wrapper" class="rev_slider_wrapper fullwidthbanner-container" data-alias="blue-replace" data-source="gallery" style="margin:0px auto;background:transparent;padding:0px;margin-top:0px;margin-bottom:0px;">
    <!-- START REVOLUTION SLIDER 5.4.8 fullwidth mode -->
    <div id="rev_slider_126_1" class="rev_slider fullwidthabanner iq-home01" style="display:none;" data-version="5.4.8">
        <ul class="text-space">
            <!-- SLIDE  -->
            <li data-index="rs-432">
                <!-- MAIN IMAGE -->
                <img src="images/banner1.jpg" alt="" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-no-retina>
                <!-- LAYERS -->
                <!-- LAYER NR. 1 -->
                <div class="tp-caption tp-resizeme" id="slide-432-layer-1" data-x="center" data-hoffset="" data-y="center" data-voffset="2" data-width="['auto']" data-height="['auto']" data-type="text" data-responsive_offset="on" data-frames='[{"delay":1000,"speed":1500,"frame":"0","from":"y:50px;opacity:0;","to":"o:1;","ease":"Power2.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]' data-textAlign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 5; white-space: nowrap; font-size: 70px; line-height: 80px; font-weight: 600; color: #ffffff; letter-spacing: 0px;font-family:Raleway;">
                    Comprehensive  <i class="italic">Keyboarding</i> </div>
                <!-- LAYER NR. 2 -->
                <div class="tp-caption tp-resizeme p-text" id="slide-432-layer-24" data-x="center" data-hoffset="" data-y="center" data-voffset="77" data-width="['auto']" data-height="['auto']" data-type="text" data-responsive_offset="on" data-frames='[{"delay":1500,"speed":1500,"frame":"0","from":"y:-50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]' data-textAlign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 6; white-space: nowrap; font-size: 20px; line-height: 22px; font-weight: 400; color: #ffffff; letter-spacing: 0px;font-family:Raleway;">Lorem Ipsum is simply dummy text </div>
                <!-- LAYER NR. 3 -->
                <div class="tp-caption button tp-resizeme" id="slide-432-layer-30" data-x="center" data-hoffset="" data-y="center" data-voffset="146" data-width="['auto']" data-height="['auto']" data-type="button" data-responsive_offset="on" data-responsive="off" data-frames='[{"delay":2200,"speed":1500,"frame":"0","from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;","mask":"x:0px;y:[100%];s:inherit;e:inherit;","to":"o:1;","ease":"Power2.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"},{"frame":"hover","speed":"0","ease":"Linear.easeNone","to":"o:1;rX:0;rY:0;rZ:0;z:0;","style":""}]' data-textAlign="['inherit','inherit','inherit','inherit']" data-paddingtop="[10,10,10,10]" data-paddingright="[30,30,30,30]" data-paddingbottom="[10,10,10,10]" data-paddingleft="[30,30,30,30]" style="z-index: 7; box-sizing: border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;cursor:pointer;">Know More </div>

            </li>
            <!-- SLIDE  -->
            <li  data-index="rs-433" data-transition="random-static,random-premium,random" data-slotamount="default,default,default,default" data-hideafterloop="0" data-hideslideonmobile="off" data-randomtransition="on" data-easein="default,default,default,default" data-easeout="default,default,default,default" data-masterspeed="default,default,default,default" data-thumb="revolution/assets/100x50_e75fd-b.jpg" data-rotate="0,0,0,0" data-saveperformance="off" class="b1-video text-left" data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                <!-- MAIN IMAGE -->
                <img src="images/banner2.jpg" alt="" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-no-retina>
                <!-- LAYERS -->
                <!-- LAYER NR. 10 -->
                <div class="tp-caption tp-resizeme" id="slide-433-layer-1" data-x="60" data-y="center" data-voffset="1" data-width="['auto']" data-height="['auto']" data-type="text" data-responsive_offset="on" data-frames='[{"delay":1000,"speed":1500,"frame":"0","from":"x:50px;opacity:0;","to":"o:1;","ease":"Power2.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]' data-textAlign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 5; white-space: nowrap; font-size: 70px; line-height: 80px; font-weight: 600; color: #ffffff; letter-spacing: 0px;font-family:Raleway;">
                    Career Prep & <i class="italic">Professionalism </i> </div>
                <!-- LAYER NR. 11 -->
                <div class="tp-caption tp-resizeme p-text" id="slide-433-layer-24" data-x="60" data-y="center" data-voffset="68" data-width="['auto']" data-height="['auto']" data-type="text" data-responsive_offset="on" data-frames='[{"delay":1500,"speed":1500,"frame":"0","from":"x:-50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]' data-textAlign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 6; white-space: nowrap; font-size: 20px; line-height: 22px; font-weight: 400; color: #ffffff; letter-spacing: 0px;font-family:Raleway;">Lorem Ipsum is simply dummy text </div>
                <!-- LAYER NR. 12 -->

            </li>
            <!-- SLIDE  -->
            <li data-index="rs-434" data-transition="random-static,random-premium,random" data-slotamount="default,default,default,default" data-hideafterloop="0" data-hideslideonmobile="off" data-randomtransition="on" data-easein="default,default,default,default" data-easeout="default,default,default,default" data-masterspeed="default,default,default,default" data-thumb="revolution/assets/100x50_07fca-c.jpg" data-rotate="0,0,0,0" data-saveperformance="off" class="b1-video text-right" data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                <!-- MAIN IMAGE -->
                <img src="images/banner3.jpg" alt="" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-no-retina>
                <!-- LAYERS -->
                <!-- LAYER NR. 19 -->
                <div class="tp-caption tp-resizeme" id="slide-434-layer-1" data-x="right" data-hoffset="60" data-y="center" data-voffset="-4" data-width="['auto']" data-height="['auto']" data-type="text" data-responsive_offset="on" data-frames='[{"delay":1000,"speed":1500,"frame":"0","from":"x:-50px;opacity:0;","to":"o:1;","ease":"Power2.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]' data-textAlign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 5; white-space: nowrap; font-size: 70px; line-height: 80px; font-weight: 600; color: #ffffff; letter-spacing: 0px;font-family:Raleway;">
                    Standardized Testing <i class="italic"></i>  Preparation</div>
                <!-- LAYER NR. 20 -->
                <div class="tp-caption tp-resizeme p-text" id="slide-434-layer-24" data-x="right" data-hoffset="60" data-y="center" data-voffset="78" data-width="['auto']" data-height="['auto']" data-type="text" data-responsive_offset="on" data-frames='[{"delay":1500,"speed":1500,"frame":"0","from":"x:50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]' data-textAlign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 6; white-space: nowrap; font-size: 20px; line-height: 22px; font-weight: 400; color: #ffffff; letter-spacing: 0px;font-family:Raleway;">Lorem Ipsum is simply dummy text </div>

            </li>
        </ul>
        <div class="tp-bannertimer tp-bottom" style="visibility: hidden !important;"></div>
    </div>
</div>
<!--=================================
Banner -->


<!--=================================
Main Content -->
<div class="main-content">
    <!--=================================
Blog -->
    <section class="white-bg">
        <div class="container-fluid">
            <div class="row no-gutter">
                <div class="col-lg-4 col-sm-12">
                    <div class="iq-feature4 text-center iq-font-white iq-ptb-60 iq-pl-50 iq-pr-50 iq-over-black-80">
                        <div class="content-blog iq-blog-entry">
                            <i class="icon-blog"><img src="images/blog2.png"></i>
                            <h4 class="iq-font-green iq-tw-6 iq-mt-20 iq-mb-10">Hindi Typewriter Keyboard Layout</h4>
                            <p class="text-gray">Lorem Ipsum is simply dummy text ever sincehar the 1500s, when an unknownshil printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries.
                                <span class="read-more"><a href="#">Read More<i class="fa fa-angle-double-right" aria-hidden="true"></i></a></span>
                            </p>

                        </div>
                        <a href="#"><div class="feature-img" style="background-image: url('images/b1.jpg')"></div></a>
                    </div>
                </div>
                <div class="col-lg-4 col-sm-12">
                    <div class="iq-feature4 text-center iq-font-white iq-ptb-60 iq-pl-50 iq-pr-50 iq-over-green-80">
                        <div class="content-blog iq-blog-entry">
                            <i class="icon-blog"><img src="images/blog1.png"></i>
                            <h4 class="iq-font-white iq-tw-6 iq-mt-20 iq-mb-10">Inscript ( Indian Script ) Hindi Keyboard</h4>
                            <p class="text-gray">Lorem Ipsum is simply dummy text ever sincehar the 1500s, when an unknownshil printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries.
                                <span class="read-more-white"><a href="#">Read More<i class="fa fa-angle-double-right" aria-hidden="true"></i></a></span>
                            </p>

                        </div>
                        <a href="#"><div class="feature-img" style="background-image: url('images/b2.jpg')"></div></a>

                    </div>
                </div>
                <div class="col-lg-4 col-sm-12">
                    <div class="iq-feature4 text-center iq-font-white iq-ptb-60 iq-pl-50 iq-pr-50 iq-over-black-80">
                        <div class="content-blog iq-blog-entry">
                            <i class="icon-blog"><img src="images/blog2.png"></i>
                            <h4 class="iq-font-green iq-tw-6 iq-mt-20 iq-mb-10"> Hindi Typewriter Keyboard Layout</h4>
                            <p class="text-gray">Lorem Ipsum is simply dummy text ever sincehar the 1500s, when an unknownshil printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries.
                                <span class="read-more"><a href="#">Read More<i class="fa fa-angle-double-right" aria-hidden="true"></i></a></span>
                            </p>

                        </div>
                        <a href="#"></a><div class="feature-img" style="background-image: url('images/b1.jpg')"></div></a>
                    </div>
                </div>
            </div>
        </div>
    </section>  
    <!--=================================
Blog -->
    <!--=================================
Work Process -->
    <section class="overview-block-ptb iq-hide">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <div class="heading-title text-center">
                        <h2 class="title iq-tw-6">Typingknowledgedesk</h2>
                        <p>We welcome all of you on our website Typingknowledgedesk.com, Our Vision in creating this website was to deliver High-Quality Performance, Speed and Versatile Functions, To achieve our ends our dedicated team is working day and night to give you a very efficient website which can save your enormous time and provide you with much more features in comparison to other Typing websites. What separates us from other websites is the zeal to create the website with creativity. One of the Great things about our website is we created this with utter simplicity and no Nonsense. We made this website in a very simple design and simpler navigation panel so, that you need not to get distracted by turmoil, unlike other typing websites.</p>
                    </div>
                </div>
            </div>

        </div>
    </section>
    <!--=================================
Work Process -->
    <!--=================================
Skill 1 -->
    <section class="creative-are">
        <div class="container-fluid">
            <div class="row row-eq-height">
                <div class="col-md-6 overview-block-ptb visible-md visible-lg iq-bg jarallax" style="background-image: url('images/typing.jpg'); background-position: left center;">
                </div>
                <div class="col-md-6 iq-pall-80 grey-bg overview-block-ptb bg">
                    <div class="row">
                        <div class="col-sm-12">
                            <h4 class="small-title text-grey iq-tw-6">Online Typing Test</h4>
                            <p>If you believe you have become a pro in typing don’t brag yourself put your skills on the test. Test your typing skills on our website’s Typing Test page to be sure about it.</p>
                            <p class="iq-mt-20">We designed this Typing Test to be the most accurate, comprehensive review of your typing skills.</p>
                            <p class="typo-style1">Dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                            <br>
                            <div class="card">

                                <div class="card-header">Welcome to Typing knowledge desk, Test your Typing speed with multilingual Regional  &  International Language</div>
                                <div class="card-body">
                                    <select class="form-control" id="">
                                        <option selected="" value="#">English</option>
                                        <option>Hindi</option>
                                        <option>Punjabi</option>
                                        <option>Bengali</option>
                                        <option>Telugu Typing</option>
                                    </select>
                                    <select class="form-control" id="">
                                        <option>1 minute test</option>
                                        <option>2 minute test</option>
                                        <option>3 minute test</option>
                                        <option>4 minute test</option>
                                        <option>5 minute test</option>
                                        <option>10 minute test</option>
                                    </select>
                                    <select class="form-control" id="">
                                        <option selected="" value="#">Aesop’s fables</option>
                                        <option value="#">Rules of Baseball</option>
                                        <option value="#">Space cowboys</option>
                                        <option value="#">Tigers in the Wild</option>
                                        <option value="#">The Wonderful Wizard of Oz</option>
                                        <option value="#">Zebra - Africa's striped horse</option>
                                        <option value="#">The Enchanted Typewriter</option>
                                        <option value="#">Deutsch</option>
                                        <option value="#">Français</option>
                                        <option value="#">Español</option>
                                        <option value="#">Português</option>
                                        <option value="#">Nederland</option>
                                        <option value="#">Italiano</option>
                                        <option value="#">Svenska</option>
                                        <option value="#">Suomi</option>
                                    </select>

                                </div> 
                                <div class="card-footer ">
                                    <button class="button iqz " onclick="#">Start Typing Test</button>
                                </div>

                            </div>
                        </div>
                        <div class="col-sm-4"></div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--================================
Skill 1  -->

    <!--=================================
Services -->
    <section class="overview-block-ptb popup-gallery">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="heading-title text-center">
                        <h2 class="title iq-tw-6">Our Services</h2>
                        <p>Lorem Ipsum is simply dummy text ever sincehar the 1500s, when an unknownshil printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries.</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid iq-hide">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 text-center">
                    <div class="isotope iq-columns-4" style="position: relative; height: 662.126px;">
                        <!--service 1-->
                        <div class="iq-grid-item photography" style="position: absolute; left: 0px; top: 0px;">
                            <div class="iq-portfolio-01">
                                <div class="iq-portfolio-img">
                                    <img class="img-fluid" src="images/t1.jpg" alt="#">
                                </div>
                                <div class="iq-portfolio-content">
                                    <a href="#" class="title">Typing Test</a>
                                </div>
                                <ul class="iq-portfolio-icon">
                                    <li><a href="#"><i class="fa fa-link"></i></a></li>
                                </ul>
                            </div>
                        </div>
                        <!--service 2-->
                        <div class="iq-grid-item photography" style="position: absolute; left: 0px; top: 0px;">
                            <div class="iq-portfolio-01">
                                <div class="iq-portfolio-img ">
                                    <img class="img-fluid" src="images/t2.jpg" alt="#">
                                </div>
                                <div class="iq-portfolio-content">
                                    <a href="#" class="title">Online Keyboard</a>
                                </div>
                                <ul class="iq-portfolio-icon">
                                    <li><a href="#"><i class="fa fa-link"></i></a></li>
                                </ul>
                            </div>
                        </div>
                        <!--service 3-->
                        <div class="iq-grid-item photography" style="position: absolute; left: 0px; top: 0px;">
                            <div class="iq-portfolio-01">
                                <div class="iq-portfolio-img">
                                    <img class="img-fluid" src="images/t3.jpg" alt="#">
                                </div>
                                <div class="iq-portfolio-content">
                                    <a href="#" class="title">Typing Tools</a>
                                </div>
                                <ul class="iq-portfolio-icon">
                                    <li><a href="#"><i class="fa fa-link"></i></a></li>
                                </ul>
                            </div>
                        </div>

                        <!--service 4-->
                        <div class="iq-grid-item photography" style="position: absolute; left: 0px; top: 0px;">
                            <div class="iq-portfolio-01">
                                <div class="iq-portfolio-img">
                                    <img class="img-fluid" src="images/t4.jpg" alt="#">
                                </div>
                                <div class="iq-portfolio-content">
                                    <a href="#" class="title">Translator</a>
                                </div>
                                <ul class="iq-portfolio-icon">
                                    <li><a href="#"><i class="fa fa-link"></i></a></li>
                                </ul>
                            </div>
                        </div>

                        <!--service 5-->
                        <div class="iq-grid-item photography" style="position: absolute; left: 0px; top: 0px;">
                            <div class="iq-portfolio-01">
                                <div class="iq-portfolio-img">
                                    <img class="img-fluid" src="images/t5.jpg" alt="#">
                                </div>
                                <div class="iq-portfolio-content">
                                    <a href="#" class="title">Typing Tutor</a>
                                </div>
                                <ul class="iq-portfolio-icon">
                                    <li><a href="#"><i class="fa fa-link"></i></a></li>
                                </ul>
                            </div>
                        </div>

                        <!--service 6-->
                        <div class="iq-grid-item photography" style="position: absolute; left: 0px; top: 0px;">
                            <div class="iq-portfolio-01">
                                <div class="iq-portfolio-img">
                                    <img class="img-fluid" src="images/t6.jpg" alt="#">
                                </div>
                                <div class="iq-portfolio-content">
                                    <a href="#" class="title">Converter</a>
                                </div>
                                <ul class="iq-portfolio-icon">
                                    <li><a href="#"><i class="fa fa-link"></i></a></li>
                                </ul>
                            </div>
                        </div>


                        <!--service 7-->
                        <div class="iq-grid-item photography" style="position: absolute; left: 0px; top: 0px;">
                            <div class="iq-portfolio-01">
                                <div class="iq-portfolio-img">
                                    <img class="img-fluid" src="images/t7.jpg" alt="#">
                                </div>
                                <div class="iq-portfolio-content">
                                    <a href="#" class="title">Games</a>
                                </div>
                                <ul class="iq-portfolio-icon">
                                    <li><a href="#"><i class="fa fa-link"></i></a></li>
                                </ul>
                            </div>
                        </div>
                        <!--service 8-->
                        <div class="iq-grid-item photography" style="position: absolute; left: 0px; top: 0px;">
                            <div class="iq-portfolio-01">
                                <div class="iq-portfolio-img">
                                    <img class="img-fluid" src="images/t8.jpg" alt="#">
                                </div>
                                <div class="iq-portfolio-content">
                                    <a href="#" class="title">Robust Reporting</a>
                                </div>
                                <ul class="iq-portfolio-icon">
                                    <li><a href="#"><i class="fa fa-link"></i></a></li>
                                </ul>
                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--=================================
Services -->